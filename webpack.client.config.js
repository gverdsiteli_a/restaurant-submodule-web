const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const ReactServerWebpackPlugin = require('react-server-dom-webpack/plugin');
const ReactServerWebpackPlugin = require('./plugin/ReactFlightWebpackPlugin')
  .default;
//const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const isProduction = process.env.NODE_ENV === 'production';

const cwd = process.cwd();

module.exports = {
  context: path.resolve(cwd, './'),
  mode: isProduction ? 'production' : 'development',
  devtool: isProduction ? 'source-map' : 'cheap-module-source-map',
  entry: [path.resolve(__dirname, './src/index.client.tsx')],
  output: {
    path: path.resolve(__dirname, './build'),
    filename: 'main.js',
    pathinfo: false,
    futureEmitAssets: true,
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json', '.mjs', ".css", ".scss"]
  },
  optimization: {
    minimize: isProduction ? true : false,
    //minimizer: [
    //  new TerserPlugin({
    //      extractComments: false
    //  })
    //]
  },
  module: {
    rules: [
        {
          test: /\.(js|jsx|ts|tsx)?$/,
          // use: 'babel-loader',
          use: ['babel-loader?cacheDirectory'],
          exclude: /node_modules/,
        },
        /*
        {
            test: /\.(scss|css)$/,
            use: [
                {
                    loader: MiniCssExtractPlugin.loader
                },
                {
                    loader: "css-loader",
                    options: {
                        importLoaders: 2
                    }
                },
                {
                    loader: "sass-loader",
                    options: {
                        implementation: require("sass")
                    }
                }
            ]
        },
        {
            test: /\.(png|jpg|gif|svg)$/,
            use: [
                {
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]",
                        publicPath: "../images",
                        emitFile: false
                    }
                }
            ]
        },
        {
            test: /\.(eot|ttf|woff|woff2)$/,
            use: [
                {
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]",
                        publicPath: "../fonts",
                        emitFile: false
                    }
                }
            ]
        }
        */
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: path.resolve(__dirname, './public/index.html'),
    }),
    new ReactServerWebpackPlugin({
      isServer: false,
    }),
    //new MiniCssExtractPlugin({
    //  filename: "[name].css"
    //})
  ],
};
