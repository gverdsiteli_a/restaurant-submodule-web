import React from 'react';
import { AreaContext } from './area-provider-context.server';

const { useContext } = React;

type RenderCallback = (components: React.ReactElement[]) => React.ReactNode;

interface Props {
    name: string;
    children?: React.ReactNode | RenderCallback;
}

const EMPTY_ARRAY = [] as React.ReactElement[];

export const RenderArea: React.FunctionComponent<Props> = ({
    name: areaId,
    children,
}) => {
    //@ts-ignore
    const context = useContext<typeof AreaContext>(AreaContext);
    //@ts-ignore
    const components = context.getComponents(areaId);
    return typeof children === 'function'
        ? (children as RenderCallback)(components || EMPTY_ARRAY)
        : components;
};