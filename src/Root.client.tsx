import { useState, Suspense } from 'react';
import { ErrorBoundary } from 'react-error-boundary';

import { useServerResponse } from './Cache.client';
import { LocationContext } from './contexts/location-context';
import { AreaProvider } from './contexts/area-provider-context.server';

import { BrowserRouter } from "react-router-dom";

export default function Root({ initialCache }) {
  return (
    <Suspense fallback={null}>
        <ErrorBoundary FallbackComponent={Error}>
          <Content />
        </ErrorBoundary>
    </Suspense>
  );
}

function Content() {
  const [location, setLocation] = useState({
    selectedId: null,
    isEditing: false,
    searchText: '',
  });
  const response = useServerResponse(location);
  return (
    <LocationContext.Provider value={[location, setLocation]}>
      <AreaProvider>
        <BrowserRouter>
          {response.readRoot()}
        </BrowserRouter>
      </AreaProvider>
    </LocationContext.Provider>
  );
}

function Error({ error }) {
  return (
    <div>
      <h1>Application Error</h1>
      <pre style={{ whiteSpace: 'pre-wrap' }}>{error.stack}</pre>
    </div>
  );
}
