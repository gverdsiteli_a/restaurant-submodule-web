import React from "react";
import { RenderArea } from "../../../contexts/area-render-context.server";
//import s from './Main.module.css';

interface Props {
  darkMode: any;
  //props?: React.Props<any>
}
export const Main: React.FunctionComponent<Props> = () => {
  //const {props} = props;
  return (
    <div
        className="main__container"
        //className={darkMode ?  `${s.darkmode}` : undefined}
        >
        <RenderArea name="main" />
    </div>
  );
}

export default Main;