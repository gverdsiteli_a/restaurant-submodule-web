import TextWithMarkdown from '../../cells/TextWithMarkdown/TextWithMarkdown';

export default function NotePreview({ body }) {
  return (
    <div className="note-preview">
      <TextWithMarkdown text={body} />
    </div>
  );
}
