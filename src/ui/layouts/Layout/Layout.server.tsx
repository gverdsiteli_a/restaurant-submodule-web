import React from "react";
import Header from '../../blocks/Header/Header.client.jsx';
import Navigation from '../../blocks/Nav/Navigation.server';
import Main from '../../blocks/Main/Main.server';
import Footer from '../../blocks/Footer/Footer.server';
//import s from './Layout.module.css';

interface Props {
  props:any, 
  context: any
}

const Layout: React.FunctionComponent<Props> 
  = 
  () => {
  return (
    <>
      <header className="page__header header">
        <Header/>
      </header>
      {/*
      <nav className="page__nav nav">
        <Navigation/>
      </nav>
      <main
        className="page__content main">
          <Main/>
      </main>
      <footer className="page__footer footer">
        <Footer/>
      </footer>
      */}
    </>
  )
}
export default Layout;