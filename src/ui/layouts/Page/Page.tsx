import React, { useState } from "react";
import { Content } from "../../../contexts/area-context.server";
import { useScrollPosition } from "../../../helpers/useScrollPosition";
import { useDocumentTitle } from '../../../documentTitle';

const base = "Restaurant";

export const Page: React.FunctionComponent<{
  title?: string;
  documentTitle?: string;
}> = ({ title, documentTitle, children }) => {

  const [showInHeader, setShowInHeader] = useState(false);
  useDocumentTitle(documentTitle ? `${documentTitle} | ${base}` : base);
  
  useScrollPosition({
    effect: offset => {
      setShowInHeader(offset > 100);
    },
  });
  return (
    <>
      <Content name="toolbar/left">
        {title}
      </Content>
      {title ? (
        <div style={{ margin: 0, marginBottom: 20 }}>{title}</div>
      ) : null}

      {children}
    </>
  );
};
