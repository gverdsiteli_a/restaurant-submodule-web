import { Suspense } from 'react';

import Feed from "./features/Feed/Feed.server";
import Profile from "./features/Profile/Profile.server";
import Settings from "./features/Settings/Settings.server";
import Meetings from './features/Meetings/Meetings.server';
import Layout from './ui/layouts/Layout/Layout.server'


export default function App({ selectedId, isEditing, searchText }) {
  return (
      <Meetings selectedId={selectedId} isEditing={isEditing} searchText={searchText}/>
  );
}
