import { useState, unstable_useTransition } from 'react';
import { useLocation } from '../../location-context';
import { Content } from "../../area-context.server";
import Spinner from '../Shared/Modals/Spinner/Spinner';

import { posts } from "../../../../mock/posts";

import { useEntity } from "../../../../contexts/area-loading-context.server";
import { RenderArea } from "../../../../contexts/area-render-context.server";

import { Page } from "../../../../ui/layouts/Page";

interface Props {
  post: string
}

function Post({ post }:Props) {
  return (
    <div
      style={{
        border:"1px solid black",
        display:"block",
        maxWidth:"600px"
      }}>
      <img
        // src={`https://picsum.photos/600/400?d=${Date.now()}`}
        src={post.img}
        alt={post.alt || "random image from unsplash"}
        style={{ width: "100%" }}
      />
    </div>
  );
}
export default function FeedContainer() {
  const { loading } = useEntity("/api/posts");
  if (loading) {
    return null;
  }
  return (
    <Page title="Feed" documentTitle="Feed">
        <div style={{ display:"flex", flexGrow: 0 }}>
          {posts.map(post => (
            <Post key={post.id} post={post} />
          ))}
        </div>
    </Page>
  );
}
