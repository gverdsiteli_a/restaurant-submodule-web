import { Suspense } from 'react';

import FeedContainer from './Feed-Container/FeedContainer.client';

export default function Feed() {
  return (
        <div>
          <FeedContainer />
        </div>
  );
}
