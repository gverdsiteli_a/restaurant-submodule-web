import { Suspense } from 'react';

import Note from '../../ui/blocks/Note/Note.server';
import NoteList from '../../ui/blocks/Note/NoteList.server';
import NoteSkeleton from '../../ui/blocks/Note/NoteSkeleton';
import NoteListSkeleton from '../../ui/blocks/Note/NoteListSkeleton';
import EditButton from '../../ui/elements/EditButton/EditButton.client';
import SearchField from '../../ui/elements/SearchField/SearchField.client';
//import Layout from './Layout.server';
//import Feed from "./Feed.server";
//import Profile from "./features/Profile/Profile.server";
//import Settings from "./features/Settings/Settings.server";

export default function Meetings({ selectedId, isEditing, searchText }) {
  return (
    <div className="main">
      <section className="col sidebar">
        <section className="sidebar-header">
          <img
            className="logo"
            src="logo.svg"
            width="22px"
            height="20px"
            alt=""
            role="presentation"
          />
          <strong>Restaurant</strong>
        </section>
        <section className="sidebar-menu" role="menubar">
          <SearchField />
          <EditButton noteId={null}>New</EditButton>
        </section>
        <nav>
          <Suspense fallback={<NoteListSkeleton />}>
            <NoteList searchText={searchText} />
          </Suspense>
        </nav>
      </section>
      <section key={selectedId} className="col note-viewer">
        <Suspense fallback={<NoteSkeleton isEditing={isEditing} />}>
          <Note selectedId={selectedId} isEditing={isEditing} />
        </Suspense>
      </section>
    </div>
  );
}
